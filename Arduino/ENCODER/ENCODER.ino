/* Encoder Library - Basic Example
 * http://www.pjrc.com/teensy/td_libs_Encoder.html
 *
 * This example code is in the public domain.
 */
#define INITIAL_SEQUENCE 300
#define MOTOR_PIN 10
#define ENCODER_PIN_A 2
#define ENCODER_PIN_B 3
#define TIC_TO_DEG -0.09
#define MICRO_TO_SEC 1000000.0

#include <Servo.h>
#include <Encoder.h>

Servo esc;

unsigned long Time=0;
unsigned long oldTime=0;
double deltaTime=0;

double Pos = 0;
double oldPos = -999; 
double deltaPos = 0;

double Vel = 0;
double oldVel = 0;
double deltaVel = 0;

double Acc = 0;

int throttle = 1000;
char rx_byte = 0;
char command = 0;

double Kp = 0;
double Ki = 0;
double Kd = 0;
double Kf = 0;
 
Encoder myEnc(ENCODER_PIN_A, ENCODER_PIN_B);

void setup() 
{
  Serial.begin(9600);
  pinMode(MOTOR_PIN, OUTPUT);
  esc.attach(MOTOR_PIN);
  Serial.println("Basic Encoder Test:");
  MotorCalibrate();
}


void loop() 
{

  
  // get the current time
  Time = micros();
  //position converted to degrees
  Pos = myEnc.read() * TIC_TO_DEG;

  //change in time converted from microseconds to seconds
  deltaTime = (Time - oldTime) / MICRO_TO_SEC;
  //change in position
  deltaPos = Pos - oldPos;

  //velocity - degrees per second
  Vel = deltaPos / deltaTime;
  //change in velocity
  deltaVel = Vel - oldVel;

  //acceleration - degrees per second per second
  Acc = deltaVel / deltaTime;






  command = getCommand();

  switch (command)
  {
    case '1':
      throttle = 1000;
      break;
    case '2':
      throttle = 1125;
      break;
    case '3':
      throttle = 1250;
      break;
    case '4':
      throttle = 1375;
      break;
    case '5':
      throttle = 1500;
      break;
    case '6':
      throttle = 1625;
      break;
    case '7':
      throttle = 1750;
      break;
    case '8':
      throttle = 1875;
      break;
    case '9':
      throttle = 2000;
      break;  

    case '+':
      throttle += 1;
      break;

     case '-':
      throttle -= 1;
      break;
    
    case '0':  
    default:    
      break;
  }

 
  esc.write(throttle);

  
  PrintStats();




  oldTime = Time;
  oldPos = Pos;
  oldVel = Vel;
  
}

void PrintStats()
{
  Serial.println("");
  Serial.print("  PWM:");
  Serial.print(throttle);
  Serial.print("   LOOP TIME:");
  Serial.print(deltaTime);
  Serial.print("   POSITION:");
  Serial.print(Pos);
  Serial.print("   RPM:");
  Serial.print(Vel/10.0);
  Serial.print("   DEG/SEC:");
  Serial.print(Vel);
  Serial.print("   DEG/SEC/SEC:");
  Serial.print(Acc);
}


char getCommand()
{
  if (Serial.available() > 0)
  {
    rx_byte = Serial.read();

    Serial.println(rx_byte);
    return rx_byte;
  }
  return '0';
}

void MotorCalibrate()
{
 Serial.println("FULL FORWARD CALIBRATE");
    for (int i=0; i<= INITIAL_SEQUENCE; i++)
    {
      esc.write(2000);     
    }
    Serial.println("FULL FORWARD CALIBRATE COMPLETE");

    Serial.println("FULL STOP CALIBRATE");
    for (int i=0; i<= 3000; i++)
    {
      esc.write(1000);     
    }
    Serial.println("FULL STOP CALIBRATE COMPLETE");
   
  
}





  /*
  Serial.println("");
  Serial.print(deltaTime);
  Serial.print("              ");
  Serial.print(Pos);
  Serial.print("              ");
  Serial.print(Vel);
  Serial.print("              ");
  Serial.print(Acc);
  */
  /*
  long newPosition = myEnc.read();
  if (newPosition != oldPosition) 
  {
    Serial.println(newPosition-oldPosition); //newPosition
    oldPosition = newPosition;   
  }
  */
