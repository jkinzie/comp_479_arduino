

const int motorPin = 10;

int throttle = 1000;
char rx_byte = 0;
char command = 0;


void setup()
{
  Serial.begin(9600);
  pinMode(motorPin, OUTPUT);
}

void loop()
{
  command = getCommand();

  switch (command)
  {
    case '1':
      throttle = 1000;
      break;
    case '2':
      throttle = 1125;
      break;
    case '3':
      throttle = 1250;
      break;
    case '4':
      throttle = 1375;
      break;
    case '5':
      throttle = 1500;
      break;
    case '6':
      throttle = 1625;
      break;
    case '7':
      throttle = 1750;
      break;
    case '8':
      throttle = 1875;
      break;
    case '9':
      throttle = 2000;
      break;  

    case '+':
      throttle += 1;
      break;

     case '-':
      throttle -= 1;
      break;
    
    case '0':  
    default:    
      break;
  }

  digitalWrite(motorPin, HIGH);
  delayMicroseconds(throttle);
  digitalWrite(motorPin, LOW);
  delayMicroseconds(throttle);
  Serial.println(throttle);

}



char getCommand()
{
  if (Serial.available() > 0)
  {
    rx_byte = Serial.read();

    Serial.println(rx_byte);
    return rx_byte;

    /*
    if ((rx_byte >= '0') && (rx_byte <= '9'))
    {
      Serial.print("Number received: ");
      Serial.println(rx_byte);
      return rx_byte;
    }
    else
    {
      Serial.println("Not a number.");
    }
    */
  }
  return '0';
}
