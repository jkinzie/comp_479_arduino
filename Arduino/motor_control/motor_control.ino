const int buttonPin1 = 2;
const int buttonPin2 = 3;
const int ledPin = 13;
const int motorPin = 10;
const double thetaInitial = 4.71238898038;
const double thetaStep = .001;
const double thetaReset = 4.71238898038+(10*3.14159265359);
double theta = 4.71238898038;

double throttle = 0.0;

const int initialSequence = 500;

void setup()
{
  Serial.begin(9600);
  pinMode(motorPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);

  Serial.println("");
  Serial.println("");

  delay(5000);

  if (HIGH == digitalRead(buttonPin2))
  {
    digitalWrite(ledPin, HIGH);
    for (int i=0; i<=initialSequence; i++)
    {
      digitalWrite(motorPin, HIGH);
      delayMicroseconds(2000); 
      digitalWrite(motorPin, LOW);
      delayMicroseconds(2000);      
    }

    for (int i=0; i<=initialSequence; i++)
    {
      digitalWrite(motorPin, HIGH);
      delayMicroseconds(1000); 
      digitalWrite(motorPin, LOW);
      delayMicroseconds(1000);      
    }

    for (int i=0; i<=1000; i++)
    {
      Serial.println(i);
      digitalWrite(motorPin, HIGH);
      delayMicroseconds(1000); 
      digitalWrite(motorPin, LOW);
      delayMicroseconds(1000);      
    }
    digitalWrite(ledPin, LOW);
  }

  if (HIGH == digitalRead(buttonPin1))
  {   
    
    Serial.println("FULL FORWARD CALIBRATE");
    digitalWrite(ledPin, HIGH);
    for (int i=0; i<= initialSequence; i++)
    {
      digitalWrite(motorPin, HIGH);
      delayMicroseconds(2000); 
      digitalWrite(motorPin, LOW);
      delayMicroseconds(2000);      
    }
    Serial.println("FULL FORWARD CALIBRATE COMPLETE");
    digitalWrite(ledPin, LOW);
      
    digitalWrite(ledPin, HIGH);
    Serial.println("FULL STOP CALIBRATE");
    for (int i=0; i<= 3000; i++)
    {
      digitalWrite(motorPin, HIGH);
      delayMicroseconds(1000); 
      digitalWrite(motorPin, LOW);
      delayMicroseconds(1000);      
    }
    Serial.println("FULL STOP CALIBRATE COMPLETE");
    digitalWrite(ledPin, LOW);  
  } 
}

void loop()
{  
  if(thetaReset < theta)
  {
    //Serial.println("");
    //Serial.println(theta,5);
    //Serial.println(thetaReset,5);
    //Serial.println(sin(theta)*500,5);
    //Serial.println("");
    
    theta = thetaInitial+thetaStep;
    //delay(5000);
  }

  
  
  throttle = 1500+(sin(theta)*500);
  theta += thetaStep;
  
  Serial.println(throttle);
  
  digitalWrite(motorPin, HIGH);
  delayMicroseconds(throttle); 
  digitalWrite(motorPin, LOW);
  delayMicroseconds(throttle);    
 

  /*
  if (HIGH == digitalRead(buttonPin1))
  {
    digitalWrite(ledPin, HIGH);
    digitalWrite(motorPin, HIGH);
    delayMicroseconds(2000); 
    digitalWrite(motorPin, LOW);
    delayMicroseconds(2000);    
  }
  else if (HIGH == digitalRead(buttonPin2))
  {
    digitalWrite(motorPin, HIGH);
    delayMicroseconds(1000); 
    digitalWrite(motorPin, LOW);
    delayMicroseconds(1000);    
  }
  else
  {
    digitalWrite(ledPin, LOW);
    digitalWrite(motorPin, HIGH);
    delayMicroseconds(1200); 
    digitalWrite(motorPin, LOW);
    delayMicroseconds(1200);    
  }
  */
  

  /*
  if (HIGH == digitalRead(buttonPin1) || HIGH == digitalRead(buttonPin2))
  {
    digitalWrite(ledPin, HIGH);
  }
  else
  {
    digitalWrite(ledPin, LOW);
  }
  */

  /*
  digitalWrite(motorPin, HIGH);
  delayMicroseconds(1100); 
  digitalWrite(motorPin, LOW);
  delayMicroseconds(1100);  
  */
  
  /*
  if (HIGH == digitalRead(buttonPin1))
  {
    //digitalWrite(ledPin, HIGH);
    //Serial.println("1");
    digitalWrite(motorPin, HIGH);
    delayMicroseconds(2000); 
    digitalWrite(motorPin, LOW);
    delayMicroseconds(2000);    
  }
  else
  {
    //digitalWrite(ledPin, LOW);
    //Serial.println("-1");
    digitalWrite(motorPin, HIGH);
    delayMicroseconds(1000); 
    digitalWrite(motorPin, LOW);
    delayMicroseconds(1000);
  }
  */
}
